
	

function timer_proc (pos:p_corner_position):gboolean; cdecl;
begin
	make_step;
	gtk_widget_queue_draw(GTK_WIDGET(pos^.da));
	
	timer_proc := TRUE;
end;

procedure timer_toggled (	toggle_button:PGtkToggleButton;
							pos:p_corner_position
						); cdecl;
var
	active:gboolean;
begin
	active := gtk_toggle_button_get_active(toggle_button);
	if (active and (pos^.timer = 0)) then
	begin
		pos^.timer := g_timeout_add(
			trunc(
				gtk_adjustment_get_value(pos^.timer_adjustment)
				),
			TGSourceFunc(@timer_proc),
			pos
			);
	end
	else if ((not active) and (pos^.timer <> 0)) then
	begin
		g_source_remove (pos^.timer);
		pos^.timer := 0;
	end;
end;

procedure timer_value_changed
			(adj:PGtkAdjustment; pos:p_corner_position);
begin
	if (pos^.timer <> 0) then
	begin
		g_source_remove (pos^.timer);
		pos^.timer := g_timeout_add(
			trunc(
				gtk_adjustment_get_value(adj)
				),
			TGSourceFunc(@timer_proc),
			pos
			);
	end;
end;
