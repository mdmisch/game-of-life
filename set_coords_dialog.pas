Unit set_coords_dialog;

interface
Uses glib2,gtk2;

type
	set_coords_proc_t = procedure (data:pointer; x,y:gint64);
	get_coords_proc_t = procedure (data:pointer; x,y:Pgint64);
	get_dialog_emitted_t = function (data:pointer):boolean;
	set_dialog_emitted_t = procedure (data:pointer; b:boolean);

	set_coords_params = record
		main_window:PGtkWindow;
		set_coords_proc:set_coords_proc_t;
		get_coords_proc:get_coords_proc_t;
		get_dialog_emitted:get_dialog_emitted_t;
		set_dialog_emitted:set_dialog_emitted_t;
		proc_data:pointer;
	end;
	p_set_coords_params = ^set_coords_params;
	
procedure on_set_coords_button(	button:PGtkButton;
								params:p_set_coords_params); cdecl;
	
implementation
type
	dialog_related_t = record
		x_spin:PGtkWidget; {PGtkSpinButton}
		y_spin:PGtkWidget; {PGtkSpinButton}
		
		params:p_set_coords_params;
	end;
	p_dialog_related_t = ^dialog_related_t;

procedure on_dialog_destroy(dialog:PGtkDialog;
							related:p_dialog_related_t
							); cdecl;
begin
	with related^.params^ do
		set_dialog_emitted(proc_data, false);
	
	g_free(related);
end;

procedure on_dialog_response(	dialog:PGtkWidget;
								rid:gint;
								related:p_dialog_related_t
							); cdecl;
begin
	if (rid = GTK_RESPONSE_OK) then with related^ do
		params^.set_coords_proc
		(
			params^.proc_data,
			trunc(gtk_spin_button_get_value(GTK_SPIN_BUTTON(x_spin))),
			trunc(gtk_spin_button_get_value(GTK_SPIN_BUTTON(y_spin)))
		);
	gtk_widget_destroy(dialog);
end;

procedure on_set_coords_button(	button:PGtkButton;
								params:p_set_coords_params); cdecl;
var
	new_dialog_related:p_dialog_related_t;
	cur_x, cur_y:gint64;
	
	x_label, y_label:PGtkWidget;
	hbox:PGtkWidget;
	dialog:PGtkWidget;
begin
	if (not params^.get_dialog_emitted(params^.proc_data)) then
	begin
		new_dialog_related := g_malloc0(sizeof(new_dialog_related^));
		
		new_dialog_related^.params := params;
		
		with new_dialog_related^ do
		begin
			dialog := gtk_dialog_new_with_buttons(
				'Move gametable coordinates dialog',
				params^.main_window,
				GTK_DIALOG_MODAL,
				GTK_STOCK_OK,
				GTK_RESPONSE_OK,
				GTK_STOCK_CANCEL,
				GTK_RESPONSE_CANCEL,
				nil
				);
			gtk_window_set_resizable(GTK_WINDOW(dialog), FALSE);			
			gtk_widget_set_size_request(dialog, 400, -1);
			
			g_signal_connect(	dialog,
								'destroy',
								G_CALLBACK(@on_dialog_destroy),
								new_dialog_related
							);
			g_signal_connect(	dialog,
								'response',
								G_CALLBACK(@on_dialog_response),
								new_dialog_related
							);
			
			
			x_label := gtk_label_new('x:');
			y_label := gtk_label_new('y:');
			


			
			hbox := gtk_hbox_new(FALSE,0);

			gtk_box_pack_start(GTK_BOX(GTK_DIALOG(dialog)^.vbox),
								hbox,
								FALSE,
								FALSE,
								0
								);
			
			params^.get_coords_proc(params^.proc_data, @cur_x, @cur_y);
			
			x_spin :=  gtk_spin_button_new_with_range(
												G_MININT64,
												G_MAXINT64,
												1
											);
			gtk_spin_button_set_value(GTK_SPIN_BUTTON(x_spin), cur_x);
			
			y_spin :=  gtk_spin_button_new_with_range(
												G_MININT64,
												G_MAXINT64,
												1
											);
			gtk_spin_button_set_value(GTK_SPIN_BUTTON(y_spin), cur_y);
			
			gtk_box_pack_start(GTK_BOX(hbox), x_label, FALSE, FALSE, 1);
			gtk_box_pack_start(GTK_BOX(hbox), x_spin, TRUE, TRUE, 1);
			gtk_box_pack_start(GTK_BOX(hbox), y_label, FALSE, FALSE, 1);
			gtk_box_pack_start(GTK_BOX(hbox), y_spin, TRUE, TRUE, 1);
			
			gtk_widget_show_all(dialog);
		end;
	end;
	
	params^.set_dialog_emitted(params^.proc_data,true);
end;



begin

end.
