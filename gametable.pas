Unit gametable;

interface
Uses glib2;

type
cell_t = record
	data:array [0..15] of word;
	history:array [0..15] of word;
end;

p_cell_t = ^cell_t;

cell_coord_t = record
	x:gint64;
	y:gint64;
end;

p_cell_coord_t = ^cell_coord_t;

procedure gametable_init;
procedure gametable_deinit;

function get_cell(x,y:gint64;force:boolean):p_cell_t;
function make_step:guint;
function remove_empty(coord:p_cell_coord_t):boolean;

procedure gametable_clear;

procedure gametable_save(fname:pchar);
procedure gametable_load(fname:pchar);

implementation

function hash_cell_coord(coord:p_cell_coord_t):guint; cdecl;
begin
	if (coord = nil) then
		hash_cell_coord := 0
	else
		hash_cell_coord :=	((coord^.x and $0000000F) or
							((coord^.x and $000000F0) shl 4) or
							((coord^.x and $00000F00) shl 8) or
							((coord^.x and $0000F000) shl 12) or
							((coord^.x and $000F0000) shl 16) or
							((coord^.x and $00F00000) shl 20) or
							((coord^.x and $0F000000) shl 24) or
							((coord^.x and $F0000000) shl 28) or
							((coord^.y and $0000000F) shl 4) or
							((coord^.y and $000000F0) shl 8) or
							((coord^.y and $00000F00) shl 12) or
							((coord^.y and $0000F000) shl 16) or
							((coord^.y and $000F0000) shl 20) or
							((coord^.y and $00F00000) shl 24) or
							((coord^.y and $0F000000) shl 28) or
							((coord^.y and $F0000000) shl 32));
end;

function equal_cell_coord(	coord1:p_cell_coord_t;
							coord2:p_cell_coord_t):gboolean; cdecl;
begin
	if	(	(coord1 <> nil) and (coord2 <> nil) and
			(coord1^.x = coord2^.x) and (coord1^.y = coord2^.y)
		) then
		equal_cell_coord := TRUE
	else
		equal_cell_coord := FALSE;
end;



var
board_table:PGHashTable;

function get_cell_from_hashtable(	table:PGHashTable;
									x,y:gint64;
									force:boolean
								):p_cell_t; forward;

function get_cell(x,y:gint64;force:boolean):p_cell_t;
begin
	get_cell := get_cell_from_hashtable(board_table, x, y, force);
end;

function get_cell_from_hashtable(	table:PGHashTable;
									x,y:gint64;
									force:boolean
								):p_cell_t;
var
cell:p_cell_t;
coord:cell_coord_t;
new_key:p_cell_coord_t;
begin
	coord.x:=x;
	coord.y:=y;
	cell := g_hash_table_lookup(table, @coord);
	
	if ((cell = nil) and (force)) then
	begin
		new_key := g_malloc(sizeof(cell_coord_t));
		new_key^.x := x;
		new_key^.y := y;
		
		cell := g_malloc0(sizeof(cell_t));
		
		g_hash_table_insert(table, new_key, cell);
	end;
	
	get_cell_from_hashtable := cell;
	
end;

function remove_empty(coord:p_cell_coord_t):boolean;
var
cell:p_cell_t;
accum:word;
i:integer;
begin
	cell := g_hash_table_lookup(board_table, coord);
	
	if (cell = nil) then
	begin
		remove_empty := true;
		exit;
	end;
	
	accum := cell^.data[0];
	i := 1;
	while ((accum = 0) and (i < 16)) do
	begin
		accum := accum or cell^.data[i];
		inc(i);
	end;
	
	if (accum = 0) then
	begin
		g_hash_table_remove(board_table, coord);
		remove_empty := true;
	end
	else
		remove_empty := false;
end;

function perform_overwrite_history(	coord:p_cell_coord_t;
									cell:p_cell_t;
									data:pointer
									):gboolean; cdecl;
var
i:byte;
delete:gboolean;
begin
	delete := TRUE;
	with cell^ do
		for i := 0 to 15 do
			if ((data[i] <> 0) or (history[i] <> 0)) then
			begin
				history[i] := data[i];
				delete := FALSE;
			end;
	perform_overwrite_history := delete;
end;

procedure overwrite_history;
begin
	g_hash_table_foreach_remove	(	board_table,
									TGHRFunc(@perform_overwrite_history)
									,nil
								);	
end;

{ a step performing part }
{
type
cell_list = record
	cord:^cell_coord;
	cell:^p_cell;
	next:^cell_list;
end;

p_cell_list = ^cell_list;
p_p_cell_list = ^p_cell_list;
}
const
bits_num : array [0..7] of byte = (0,1,1,2,1,2,2,3);
	
function process_inner_line(cur, up, mid, down:word):word;
var
bit_pattern:word;
bit_counter:integer;
begin
	bit_pattern := $0002;
	while (bit_pattern < $8000) do
	begin
		bit_counter := bits_num[up and $0007];
		bit_counter := bit_counter + bits_num[mid and $0005];
		bit_counter := bit_counter + bits_num[down and $0007];
		if (bit_counter = 3) then
			cur := cur or bit_pattern
		else if (bit_counter <> 2) then
			cur := cur and not bit_pattern;
		
		bit_pattern := bit_pattern shl 1;
		up := up shr 1;
		mid := mid shr 1;
		down := down shr 1;
	end;
	process_inner_line := cur;
end;

procedure perform_step	(	coord:p_cell_coord_t;
							cell:p_cell_t;
							add_list:PGHashTable
						); cdecl;
{
		 0  *
		*+--+0
		 |  |
		0+--+*
		 *  0
		
		*    *
		 1--1
		 |  |
		 1--1
		*    *
}
var
	nw,n,ne,e,w,sw,s,se:p_cell_t;
	
	dif_coord:p_cell_coord_t;
	
	bit_counter:integer;
	
	{
	new_coord:cell_coord_t;
	exalted_coord:p_cell_coord_t;
	}
	new_cell:p_cell_t;
	
	i:integer;
	j:integer;
	
	bit_buf:word;
	{
	bit_pattern:word;
const
	pattern : word = $0007;
	}
begin
	dif_coord := g_malloc(sizeof(cell_coord_t));
	
	dif_coord^.y:=coord^.y + 1;
	dif_coord^.x := coord^.x - 1;
	nw := g_hash_table_lookup(board_table, dif_coord);
	
	inc(dif_coord^.x);
	n := g_hash_table_lookup(board_table, dif_coord);
	
	inc(dif_coord^.x);
	ne := g_hash_table_lookup(board_table, dif_coord);
	
	dec(dif_coord^.y);
	e := g_hash_table_lookup(board_table, dif_coord);
	
	dec(dif_coord^.y);
	se := g_hash_table_lookup(board_table, dif_coord);

	dif_coord^.x := coord^.x - 1;
	dif_coord^.y := coord^.y;
	w := g_hash_table_lookup(board_table, dif_coord);
	
	dec(dif_coord^.y);
	sw := g_hash_table_lookup(board_table, dif_coord);
	
	inc(dif_coord^.x);
	s := g_hash_table_lookup(board_table, dif_coord);
	
		{ process north west corner}
	if ((nw = nil) and ((cell^.history[15] and $0001) <> 0)) then
	begin
		bit_counter := 1;
		
		if (w <> nil) then
			bit_counter := bit_counter +
							bits_num[w^.history[15] shr 14];
		
		if (n <> nil) then
		begin
			if ((n^.history[0] and $0001) <> 0) then
				inc(bit_counter);
			if ((n^.history[1] and $0001) <> 0) then
				inc(bit_counter);	
		end;
		
		if (bit_counter = 3) then
		begin
			new_cell := get_cell_from_hashtable(add_list,
												coord^.x-1,
												coord^.y+1,
												true
												);

			new_cell^.data[0] := new_cell^.data[0] or $8000
	
		end;
	end;
	
		{ process south west corner }
	if ((sw = nil) and ((cell^.history[0] and $0001) <> 0)) then
	begin
		bit_counter := 1;
		
		if (w <> nil) then
			bit_counter := bit_counter +
								bits_num[w^.history[0] shr 14];
		
		if (s <> nil) then
		begin
			if ((s^.history[14] and $0001) <> 0) then
				inc(bit_counter);
			if ((s^.history[15] and $0001) <> 0) then
				inc(bit_counter);	
		end;
			
		if (bit_counter = 3) then
		begin
			new_cell := get_cell_from_hashtable(add_list,
												coord^.x - 1,
												coord^.y - 1,
												true
												);
			
			new_cell^.data[15] := new_cell^.data[15] or $8000
		end;
	end;
	
		{ process north east corner }
	if ((ne = nil) and ((cell^.history[15] and $8000) <> 0)) then
	begin
		bit_counter := 1;
		
		if (e <> nil) then
			bit_counter := bit_counter +
								bits_num[e^.history[15] and $0003];
		
		if (n <> nil) then
		begin
			if ((n^.history[0] and $8000) <> 0) then
				inc(bit_counter);
			if ((n^.history[1] and $8000) <> 0) then
				inc(bit_counter);
		end;
		
		if (bit_counter = 3) then
		begin
			new_cell := get_cell_from_hashtable(add_list,
												coord^.x + 1,
												coord^.y + 1,
												true
												);

			new_cell^.data[0] := new_cell^.data[0] or $0001
		end;
	end;
	
		{ process south east corner }
	if	((se = nil) and ((cell^.history[0] and $8000) <> 0)) then
	begin
		bit_counter := 1;
		
		if (e <> nil) then
			bit_counter := bit_counter +
								bits_num[e^.history[0] and $0003];
		
		if (s <> nil) then
		begin
			if ((s^.history[15] and $8000) <> 0) then
				inc(bit_counter);
			if ((s^.history[14] and $8000) <> 0) then
				inc(bit_counter);
		end;
		
		if (bit_counter = 3) then
		begin
			new_cell := get_cell_from_hashtable(add_list,
												coord^.x + 1,
												coord^.y - 1,
												true
												);

			new_cell^.data[15] := new_cell^.data[15] or $0001
		end;
	end;
	
		{ process north edge }
	if	(n = nil) then
	begin
		bit_buf := cell^.history[15];
		new_cell := nil;
		
		for i := 1 to 14 do
		begin
			if ((bit_buf and $0007) = $0007) then
			begin
				new_cell := get_cell_from_hashtable(add_list,
													coord^.x,
													coord^.y + 1,
													true
													);
			
				new_cell^.data[0] := new_cell^.data[0] or (1 shl i);
				
				break;
			end;
			bit_buf := bit_buf shr 1;
		end;
		
		for i := (i + 1) to 14 do
		begin
			bit_buf := bit_buf shr 1;
			if ((bit_buf and $0007) = $0007) then
				with new_cell^ do
					data[0] := data[0] or (1 shl i);
		end;
		
		if ((e = nil) or ((e^.history[15] and $0001) = 0)) then
		begin
			bit_counter := bits_num[cell^.history[15] shr 14];
			if (ne <> nil) then
			begin
				if ((ne^.history[0] and $0001) <> 0) then
					inc(bit_counter);
				if ((ne^.history[1] and $0001) <> 0) then
					inc(bit_counter);
			end;
			
			if (bit_counter = 3) then
			begin
				if (new_cell = nil) then
					new_cell := get_cell_from_hashtable(add_list,
														coord^.x,
														coord^.y + 1,
														true
														);
				new_cell^.data[0] := new_cell^.data[0] or $8000;
			end;
		end;
	end;
	
		{ process south edge }
	if (s = nil) then
	begin
		bit_buf := cell^.history[0];
		new_cell := nil;

		for i := 1 to 14 do
		begin
			if ((bit_buf and $0007) = $0007) then
			begin

				new_cell := get_cell_from_hashtable(add_list,
													coord^.x,
													coord^.y - 1,
													true
													);

				new_cell^.data[15] := new_cell^.data[15] or (1 shl i);
				
				break;
			end;
			bit_buf := bit_buf shr 1;
		end;
		
		for i := (i + 1) to 14 do
		begin
			bit_buf := bit_buf shr 1;
			if ((bit_buf and $0007) = $0007) then
				with new_cell^ do
					data[15] := data[15] or (1 shl i);
		end;
		
		if	(	(w = nil) or
				((w^.history[0] and $8000) = 0)
			) then
		begin
			bit_counter := bits_num[cell^.history[0] and $0003];
			if (sw <> nil) then
			begin
				if ((sw^.history[15] and $8000) <> 0) then
					inc(bit_counter);
				if ((sw^.history[14] and $8000) <> 0) then
					inc(bit_counter);
			end;
			
			if (bit_counter = 3) then
			begin
				if (new_cell = nil) then
					new_cell := get_cell_from_hashtable(add_list,
														coord^.x,
														coord^.y - 1,
														true
														);
				new_cell^.data[15] := new_cell^.data[15] or $0001;
			end;
		end;	
	end;
	
		{ process west edge }
	if (w = nil) then
	begin
		new_cell := nil;
		
		for i := 1 to 14 do
			if	(	((cell^.history[i-1] and $0001) <> 0) and
					((cell^.history[i] and $0001) <> 0) and
					((cell^.history[i+1] and $0001) <> 0)
				) then
			begin
				new_cell := get_cell_from_hashtable(add_list,
													coord^.x - 1,
													coord^.y,
													true
													);
				new_cell^.data[i] := new_cell^.data[i] or $8000;
				
				break;
			end;
		
		for i := (i+1) to 14 do
			if	(	((cell^.history[i-1] and $0001) <> 0) and
					((cell^.history[i] and $0001) <> 0) and
					((cell^.history[i+1] and $0001) <> 0)
				) then
				new_cell^.data[i] := new_cell^.data[i] or $8000;
		
		if ((n = nil) or ((n^.history[0] and $0001) = 0)) then
		begin
			if (nw <> nil) then
				bit_counter := bits_num[nw^.history[0] shr 14]
			else
				bit_counter := 0;
			
			if (cell^.history[15] = $0001) then
				inc(bit_counter);
			if (cell^.history[14] = $0001) then
				inc(bit_counter);
			
			if (bit_counter = 3) then
			begin
				if (new_cell = nil) then
					new_cell := get_cell_from_hashtable(add_list,
														coord^.x - 1,
														coord^.y,
														true
														);
				new_cell^.data[15] := new_cell^.data[15] or $8000;
			end;
		end;
	end;

		{ process east edge }
	if (e = nil) then
	begin
		new_cell := nil;
		
		for i := 1 to 14 do
			if	(	((cell^.history[i-1] and $8000) <> 0) and
					((cell^.history[i] and $8000) <> 0) and
					((cell^.history[i+1] and $8000) <> 0)
				) then
			begin
				new_cell := get_cell_from_hashtable(add_list,
													coord^.x + 1,
													coord^.y,
													true
													);
				new_cell^.data[i] := new_cell^.data[i] or $0001;
				
				break;
			end;
		
		for i := (i+1) to 14 do
			if	(	((cell^.history[i-1] and $8000) <> 0) and
					((cell^.history[i] and $8000) <> 0) and
					((cell^.history[i+1] and $8000) <> 0)
				) then
				new_cell^.data[i] := new_cell^.data[i] or $0001;
		
		if ((s = nil) or ((s^.history[15] and $8000) = 0)) then
		begin
			if (se <> nil) then
				bit_counter := bits_num[se^.history[15] and $0003]
			else
				bit_counter := 0;
			
			if (cell^.history[0] = $8000) then
				inc(bit_counter);
			if (cell^.history[1] = $8000) then
				inc(bit_counter);
			
			if (bit_counter = 3) then
			begin
				if (new_cell = nil) then
					new_cell := get_cell_from_hashtable(add_list,
														coord^.x + 1,
														coord^.y,
														true
														);
				new_cell^.data[0] := new_cell^.data[0] or $0001;
			end;
		end;
	end;

	{ fill current cell }
	
	{ fill north west corner }
	if (n <> nil) then
		bit_counter := bits_num[n^.history[0] and $0003]
	else
		bit_counter := 0;
	
	if ((nw <> nil) and ((nw^.history[0] and $8000) <> 0)) then
		inc(bit_counter);
	
	if (w <> nil) then
	begin
		if ((w^.history[15] and $8000) <> 0) then
			inc(bit_counter);
		if ((w^.history[14] and $8000) <> 0) then
			inc(bit_counter);
	end;
	
	if ((cell^.history[15] and $0002) <> 0) then
		inc(bit_counter);
	bit_counter := bit_counter + bits_num[cell^.history[14] and $0003];
	
	if (bit_counter = 3) then
		cell^.data[15] := cell^.data[15] or $0001
	else if (bit_counter <> 2) then
		cell^.data[15] := cell^.data[15] and not $0001;

	{ fill north east corner }
	if (n <> nil) then
		bit_counter := bits_num[n^.history[0] shr 14]
	else
		bit_counter := 0;
	
	if ((ne <> nil) and ((ne^.history[0] and $0001) <> 0)) then
		inc(bit_counter);
	
	if (e <> nil) then
	begin
		if ((e^.history[15] and $0001) <> 0) then
			inc(bit_counter);
			
		if ((e^.history[14] and $0001) <> 0) then
			inc(bit_counter);
	end;
	
	if ((cell^.history[15] and $4000) <> 0) then
		inc(bit_counter);
	bit_counter := bit_counter + bits_num[cell^.history[14] shr 14];
	
	if (bit_counter = 3) then
		cell^.data[15] := cell^.data[15] or $8000
	else if (bit_counter <> 2) then
		cell^.data[15] := cell^.data[15] and not $8000;
	
	{ fill south east corner }
	if (s <> nil) then
		bit_counter := bits_num[s^.history[15] shr 14]
	else
		bit_counter := 0;
	
	if ((se <> nil) and ((se^.history[15] and $0001) <> 0)) then
		inc(bit_counter);
	
	if (e <> nil) then
	begin
		if ((e^.history[0] and $0001) <> 0) then
			inc(bit_counter);
		if ((e^.history[1] and $0001) <> 0) then
			inc(bit_counter);
	end;
	
	if ((cell^.history[0] and $4000) <> 0) then
		inc(bit_counter);
	bit_counter := bit_counter + bits_num[cell^.history[1] shr 14];
	
	if (bit_counter = 3) then
		cell^.data[0] := cell^.data[0] or $8000
	else if (bit_counter <> 2) then
		cell^.data[0] := cell^.data[0] and not $8000;
	
	{ fill south west corner }
	if (s <> nil) then
		bit_counter := bits_num[s^.history[15] and $0003]
	else
		bit_counter := 0;
	
	if ((sw <> nil) and ((sw^.history[15] and $8000) <> 0)) then
		inc(bit_counter);
	
	if (w <> nil) then
	begin
		if ((w^.history[0] and $8000) <> 0) then
			inc(bit_counter);
		if ((w^.history[1] and $8000) <> 0) then
			inc(bit_counter);
	end;
	
	if ((cell^.history[0] and $0002) <> 0) then
		inc(bit_counter);
	bit_counter := bit_counter + bits_num[cell^.history[1] and $0003];
	
	if (bit_counter = 3) then
		cell^.data[0] := cell^.data[0] or $0001
	else if (bit_counter <> 2) then
		cell^.data[0] := cell^.data[0] and not $0001;
		
	{ fill north edge }
	if (n <> nil) then
		bit_buf := n^.history[0]
	else bit_buf := 0;
	
	cell^.data[15] := process_inner_line(	cell^.data[15],
											bit_buf,
											cell^.history[15],
											cell^.history[14]
										);
	{ fill inner square }
	for i := 14 downto 1 do
		cell^.data[i] := process_inner_line(cell^.data[i],
											cell^.history[i+1],
											cell^.history[i],
											cell^.history[i-1]
											);
	{ fill south edge }
	if (s <> nil) then
		bit_buf := s^.history[15]
	else
		bit_buf := 0;
	
	cell^.data[0] := process_inner_line(cell^.data[0],
										cell^.history[1],
										cell^.history[0],
										bit_buf
										);
	{ fill west edge }
	if (w <> nil) then
		for i := 14 downto 1 do
		begin
			bit_counter := bits_num[cell^.history[i-1] and $0003];
			if ((cell^.history[i] and $0002) <> 0) then
				inc(bit_counter);
			bit_counter := bit_counter +
								bits_num[cell^.history[i+1] and $0003];
			for j := (i+1) downto (i-1) do
				if ((w^.history[j] and $8000) <> 0) then
					inc(bit_counter);
			
			if (bit_counter = 3) then
				cell^.data[i] := cell^.data[i] or $0001
			else if (bit_counter <> 2) then
				cell^.data[i] := cell^.data[i] and not $0001;
		end
	else
		for i := 14 downto 1 do
		begin
			bit_counter := bits_num[cell^.history[i-1] and $0003];
			if ((cell^.history[i] and $0002) <> 0) then
				inc(bit_counter);
			bit_counter := bit_counter +
								bits_num[cell^.history[i+1] and $0003];
			
			if (bit_counter = 3) then
				cell^.data[i] := cell^.data[i] or $0001
			else if (bit_counter <> 2) then
				cell^.data[i] := cell^.data[i] and not $0001;
		end;
	
	{ fill east edge }
	if (e <> nil) then
		for i := 14 downto 1 do
		begin
			bit_counter := bits_num[cell^.history[i-1] shr 14];
			if ((cell^.history[i] and $4000) <> 0) then
				inc(bit_counter);
			bit_counter := bit_counter +
								bits_num[cell^.history[i+1] shr 14];
			for j := (i+1) downto (i-1) do
				if ((e^.history[j] and $0001) <> 0) then
					inc(bit_counter);
			
			if (bit_counter = 3) then
				cell^.data[i] := cell^.data[i] or $8000
			else if (bit_counter <> 2) then
				cell^.data[i] := cell^.data[i] and not $8000;
		end
	else
		for i := 14 downto 1 do
		begin
			bit_counter := bits_num[cell^.history[i-1] shr 14];
			if ((cell^.history[i] and $4000) <> 0) then
				inc(bit_counter);
			bit_counter := bit_counter +
								bits_num[cell^.history[i+1] shr 14];
			
			if (bit_counter = 3) then
				cell^.data[i] := cell^.data[i] or $8000
			else if (bit_counter <> 2) then
				cell^.data[i] := cell^.data[i] and not $8000;
		end;
	{ end }
end;

var add_list:PGHashTable;

function steal_from_add_list	(	coord:p_cell_coord_t;
									cell:p_cell_t;
									data:pointer
								):gboolean; cdecl;
begin
	g_hash_table_insert	(	board_table,
							coord,
							cell
						);
	steal_from_add_list := TRUE;
end;

function make_step:guint;
{
var
list_head:p_cell_list;
}
begin
	overwrite_history;
	g_hash_table_foreach(	board_table,
							TGHFunc(@perform_step),
							add_list);
	make_step := g_hash_table_foreach_steal(add_list,
											TGHRFunc
												(@steal_from_add_list),
											nil
											);
end;

function ghrfunc_true	(	key:gpointer;
							value:gpointer;
							data:gpointer
						):gboolean;
begin
	ghrfunc_true := TRUE;
end;

procedure gametable_clear;
begin
	g_hash_table_foreach_steal(
		add_list,
		TGHRFunc(@ghrfunc_true),
		nil
	);
	g_hash_table_foreach_remove(
		board_table,
		TGHRFunc(@ghrfunc_true),
		nil
	);
end;

procedure gametable_init;
begin
	board_table := g_hash_table_new_full( TGHashFunc(@hash_cell_coord),
										TGEqualFunc(@equal_cell_coord),
										@g_free,
										@g_free
										);
	add_list := g_hash_table_new_full(	TGHashFunc(@hash_cell_coord),
										TGEqualFunc(@equal_cell_coord),
										@g_free,
										@g_free
										);
end;

procedure gametable_deinit;
begin
	{g_hash_table_steal_all(add_list);}
	g_hash_table_destroy(add_list);
	g_hash_table_destroy(board_table);
end;

type p_text = ^text;

{$i-}

procedure perform_save(	coord:p_cell_coord_t;
						cell:p_cell_t;
						t:p_text
					); cdecl;
var i:integer;
begin
	writeln(t^, coord^.x, ' ', coord^.y);
	for i := 0 to 15 do write(t^, cell^.data[i], ' ');
	writeln(t^);
	writeln(t^);
end;

procedure gametable_save(fname:pchar);
var 
f:text;
begin
	if (fname <> nil) then
	begin
		assign(f,fname);
		if (ioresult = 0) then
		begin
			rewrite(f);
			if (ioresult = 0) then
			begin
				g_hash_table_foreach(	board_table,
										TGHFunc(@perform_save),
										@f
									);
				close(f);
			end;
		end;
		
	end;
end;

procedure gametable_load(fname:pchar);
var
	f:text;
	coord:p_cell_coord_t;
	cell:p_cell_t;
	i:integer;
begin
	gametable_clear;
	if (fname <> nil) then
	begin
		assign(f,fname);
		if (ioresult = 0) then
		begin
			reset(f);
			if (ioresult = 0) then
			begin
				while (not SeekEOF(f)) do
				begin
					coord := g_malloc(sizeof(coord^));
					readln(f, coord^.x, coord^.y);
					if (IOresult <> 0) then
						begin
							g_free(coord);
							break;
						end;
					cell := g_malloc0(sizeof(cell^));
					for i := 0 to 15 do
					begin
						read(f,cell^.data[i]);
						if (IOresult <> 0) then break;
					end;
					g_hash_table_insert(	board_table,
											coord,
											cell
										);
				end;
				close(f);
			end;
		end;
		
	end;
end;

begin

end.

