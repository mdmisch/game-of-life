Unit additional_rounding;

interface
Uses glib2;

function round_to_smaler(num:gdouble):gint64;
function div_to_less (x,y:gint64):gint64;
function mod_to_less (x,y:gint64):gint64;


implementation

function round_to_smaler(num:gdouble):gint64;
var
retval:gint64;
begin
	retval := round(trunc(num));
	if (frac(num) < 0) then
		dec(retval);
	round_to_smaler := retval;
end;

function div_to_less (x,y:gint64):gint64;
var
retval:gint64;
begin
	retval := x div y;
	if ((x mod y) < 0) then
		dec(retval);
	div_to_less:=retval;
end;

function mod_to_less (x,y:gint64):gint64;
var
retval:gint64;
begin
	retval := x mod y;
	if (retval < 0) then
		retval := retval + y;
	mod_to_less := retval;
end;


begin

end.
