
const
ALIVE_CELL_R = 0.9;
ALIVE_CELL_G = 0.1;
ALIVE_CELL_B = 0.1;

ALIVE_CELL_INDENTION = 1;

procedure draw_chunk(cr:Pcairo_t;
					pos:p_corner_position;
					x,y:gint64;
					cell:p_cell_t
					);
var
cur_rect_x, cur_rect_y : gdouble;
bit_buf:word;
i:integer;
begin
	
	cur_rect_y := (pos^.y - y*$10 -1) * TABLE_CELL_SIZE +
		ALIVE_CELL_INDENTION - pos^.idt_y;
	
	for i := 0 to 15 do
	begin
		cur_rect_x := 	(x*$10 - pos^.x) * TABLE_CELL_SIZE +
						ALIVE_CELL_INDENTION - pos^.idt_x;
		
		bit_buf := cell^.data[i];
		while (bit_buf <> 0) do 
		begin
			if ((bit_buf and $0001) <> 0) then
				cairo_rectangle(	cr,
									cur_rect_x,
									cur_rect_y,
									TABLE_CELL_SIZE -
										2 * ALIVE_CELL_INDENTION,
									TABLE_CELL_SIZE -
										2 * ALIVE_CELL_INDENTION
								);
			bit_buf := bit_buf shr 1;
			cur_rect_x := cur_rect_x + TABLE_CELL_SIZE;
		end;
		
		cur_rect_y := cur_rect_y - TABLE_CELL_SIZE;
	end;
	
end;

procedure draw_cells(	cr:Pcairo_t;
						pos:p_corner_position;
						width,height:gint);
var
cur_x,cur_y:gint64;
border_x, border_y:gint64;
start_y:gint64;
cur_cell:p_cell_t;
begin
	cairo_set_source_rgb(cr, ALIVE_CELL_R, ALIVE_CELL_G, ALIVE_CELL_B);

	border_x := div_to_less(pos^.x, $10);

	border_y := div_to_less((pos^.y - 1), $10);
	
	cur_x := border_x + (width div (TABLE_CELL_SIZE * $10)) + 1;
	start_y := border_y - (height div (TABLE_CELL_SIZE * $10)) - 1;
	
	{writeln(border_x,':',border_y);}
	
	while (cur_x >= border_x) do
	begin
		cur_y := start_y;
		
		while (cur_y <= border_y) do
		begin
			{writeln('_', cur_x, ':', cur_y);}
			
			cur_cell := get_cell(cur_x, cur_y, FALSE);
			if (cur_cell <> nil) then
				draw_chunk(cr, pos, cur_x, cur_y, cur_cell);
			
			inc(cur_y);
		end;
		
		dec(cur_x);
	end;
	
	cairo_fill(cr);
end;
