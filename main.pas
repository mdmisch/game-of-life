
Uses glib2, gtk2, gdk2, cairo, gametable, additional_rounding,
	set_coords_dialog;
{Uses sysutils;}

const
{STATUSBAR_SPACING = 1;}
MAIN_VBOX_SPACING = 0;
TABLEWIDGET_PADDING = 0;
ACTIONBAR_PADDING = 0;
STATUSBAR_PADDING = 0;

RULER_SIZE = 20;
RULER_SCALE_SIZE = 0.4;
RULER_BIG_SCALE_SIZE = 0.7;

RULER_LINE_WIDTH = 2;

RULER_R = 0.3;
RULER_G = 0.35;
RULER_B = 0.3;

TABLE_CELL_SIZE = 15;

TABLE_GRID_R = 0.5;
TABLE_GRID_G = 0.5;
TABLE_GRID_B = 0.5;

TABLE_CORNER_R = 0.05;
TABLE_CORNER_G = 0.5;
TABLE_CORNER_B = 0.5;

TABLE_CORNER_STEP = 16;

SCROLLING_STEP = 0.5;

var
{ widgets: }
window : PGtkWidget; {PGtkWindow}
statusbar: PGtkWidget; {PGtkStatusbar}
actionbar: PGtkWidget; {GtkHButtonBox}
da: PGtkWidget; {PGtkDrawingArea}
vbox: PGtkWidget; {PGtkVBox}

gotobutton: PGtkWidget; {PGtkButton}
gotobutton_params:set_coords_params;

step_button:PGtkWidget;

autostep_button:PGtkWidget;{PGtkToggleButton}
autostep_spin:PGtkWidget;{PGtkSpinButton}
autostep_adjustment:PGtkObject;{PGtkAdjustment}

clear_button: PGtkWidget; {PGtkButton}

save_button:PGtkWidget; {PGtkButton}
open_button:PGtkWidget; {PGtkButton}

{==================== current position==================}
type
corner_position = record
	x,y:int64;
	idt_x, idt_y:gdouble;
	on_moving:boolean;
	prev_x, prev_y:gdouble;
	statusbar:PGtkStatusbar;
	statusbar_context_id:guint;
	da:PGtkDrawingArea;
	dialog_emitted:boolean;
	timer:guint;
	timer_adjustment:PGtkAdjustment
end;
p_corner_position = ^corner_position;

{$I timer.inc}

procedure corner_position_init(	pos:p_corner_position;
								statusbar:PGtkStatusbar;
								da:PGtkDrawingArea;
								adj:PGtkAdjustment
								);
begin
	if (pos = nil) then
		exit;
	with pos^ do begin
		x:=-6;
		y:=-1;
		idt_x:=0.5 * TABLE_CELL_SIZE;
		idt_y:=0.5 * TABLE_CELL_SIZE;
		on_moving := false;
		dialog_emitted := false;
		timer:=0;
		timer_adjustment:=g_object_ref(G_OBJECT(adj));
		
		g_signal_connect(	timer_adjustment,
							'value-changed',
							G_CALLBACK(@timer_value_changed),
							pos
						);
	end;
	
		{writeln(G_IS_OBJECT(statusbar));}
		
		pos^.statusbar := g_object_ref(G_OBJECT(statusbar));
		pos^.statusbar_context_id :=
			gtk_statusbar_get_context_id(statusbar, 'print coords');
		
		pos^.da := g_object_ref(G_OBJECT(da));
end;

procedure corner_position_deinit(pos:p_corner_position);
begin
	g_object_unref(pos^.statusbar);
	g_object_unref(pos^.da);
	g_object_unref(pos^.timer_adjustment);
end;

procedure corner_position_set_xy(pos:p_corner_position; x,y:gint64);
begin
	pos^.x := x;
	pos^.y := y;
	pos^.idt_x := 0;
	pos^.idt_y := 0;
	gtk_widget_queue_draw(GTK_WIDGET(pos^.da));
end;

procedure corner_position_get_xy(pos:p_corner_position; x,y:Pgint64);
begin
	if (x <> nil) then
		x^ := pos^.x;
	if (y <> nil) then
		y^ := pos^.y;
end;

function corner_position_get_dialog_emitted
					(pos:p_corner_position):boolean;
begin
	corner_position_get_dialog_emitted := pos^.dialog_emitted;
end;

procedure corner_position_set_dialog_emitted
					(pos:p_corner_position; b:boolean);
begin
	pos^.dialog_emitted := b;
end;

{======================= callbacks =====================}

procedure on_destroy(window:PGtkWindow; data:pointer); cdecl;
begin
	gtk_main_quit;
end;
{
procedure on_da_destroy(da:PGtkDrawingArea;
						pos:p_corner_position
						); cdecl;
begin
	
end;
}
{ redrawing part }

{$I draw_chunk.inc}

function on_expose_event	(	da:PGtkDrawingArea;
								event:PGdkEventExpose;
								pos:p_corner_position
							):gboolean; cdecl;
var
cr:^cairo_t;
width, height:gint;
mouse_x, mouse_y:gint;

cur_line_pos:gdouble;
counter:integer;

{
var TS:TTimeStamp;
}
begin
	{
	width := gdk_window_get_width(event^.window);
	height := gdk_window_get_height(event^.window);
	}
	{
	TS:=DateTimeToTimestamp(now);
	writeln(TS.time);
	}
	
	gdk_drawable_get_size(GDK_DRAWABLE(event^.window), @width, @height);
	
	cr := gdk_cairo_create(GDK_DRAWABLE(event^.window));
	gdk_cairo_region(cr, event^.region);
	cairo_clip(cr);
	
	{ruler drawing}
	cairo_set_source_rgb(cr, RULER_R, RULER_G, RULER_B);
	cairo_set_line_width(cr, RULER_LINE_WIDTH);

	cairo_move_to(cr, 0, RULER_SIZE - 1);
	cairo_line_to(cr, width, RULER_SIZE - 1);
	cairo_move_to(cr, RULER_SIZE - 1, 0);
	cairo_line_to(cr, RULER_SIZE - 1, height);
	
	gtk_widget_get_pointer(GTK_WIDGET(da), @mouse_x, @mouse_y);
	
	cairo_move_to(cr, mouse_x, 0);
	cairo_line_to(cr, mouse_x, RULER_SIZE - 1);
	
	cairo_move_to(cr, 0, mouse_y);
	cairo_line_to(cr, RULER_SIZE - 1, mouse_y);
	
	cur_line_pos := RULER_SIZE + TABLE_CELL_SIZE - pos^.idt_x;
	counter := (TABLE_CORNER_STEP + 1 + 
		(pos^.x mod TABLE_CORNER_STEP) )
			mod TABLE_CORNER_STEP; {writeln(pos^.x, ' ', counter);}
	while (cur_line_pos <= width) do
	begin
		if (counter <> 0) then
			cairo_move_to(cr, cur_line_pos,
				(1-RULER_SCALE_SIZE)*RULER_SIZE)
		else
			cairo_move_to(cr, cur_line_pos,
				(1-RULER_BIG_SCALE_SIZE)*RULER_SIZE);
		
		cairo_line_to(cr, cur_line_pos, RULER_SIZE);
		
		cur_line_pos := cur_line_pos + TABLE_CELL_SIZE;
		counter := (counter + 1) mod TABLE_CORNER_STEP;
	end;
	
	cur_line_pos := RULER_SIZE + TABLE_CELL_SIZE - pos^.idt_y;
	counter := (TABLE_CORNER_STEP + 1 - pos^.y mod TABLE_CORNER_STEP)
		mod TABLE_CORNER_STEP;
	while (cur_line_pos <= height) do
	begin
		if (counter <> 0) then
			cairo_move_to	(	cr,
								(1-RULER_SCALE_SIZE)*RULER_SIZE,
								cur_line_pos
							)
		else
			cairo_move_to	(	cr,
								(1-RULER_BIG_SCALE_SIZE)*RULER_SIZE,
								cur_line_pos
							);
		
		cairo_line_to(cr, RULER_SIZE, cur_line_pos);
		
		cur_line_pos := cur_line_pos + TABLE_CELL_SIZE;
		counter := (counter + 1) mod TABLE_CORNER_STEP;
	end;
	
	cairo_stroke(cr);

	{limit drawing space}
	cairo_translate(cr, RULER_SIZE, RULER_SIZE);
	cairo_rectangle(cr,0,0,width - RULER_SIZE, height - RULER_SIZE);
	cairo_clip(cr);
	
	{cells drawing}
	draw_cells(	cr,
				pos,
				width - round(pos^.idt_x),
				height - round(pos^.idt_y));

	{grid drawing}
	cairo_set_source_rgb(cr, TABLE_GRID_R, TABLE_GRID_G, TABLE_GRID_B);
	cairo_set_line_width(cr, 2);
	
	{ draw ordinary vertical lines }
	cur_line_pos := TABLE_CELL_SIZE - pos^.idt_x;
	counter := (TABLE_CORNER_STEP + 1 + 
		(pos^.x mod TABLE_CORNER_STEP) )
			mod TABLE_CORNER_STEP; {writeln(pos^.x, ' ', counter);}
	while (cur_line_pos <= width - RULER_SIZE) do
	begin
		if (counter <> 0) then
		begin
			cairo_move_to(cr, cur_line_pos, 0);
			cairo_line_to(cr, cur_line_pos, height - RULER_SIZE);
		end;
		cur_line_pos := cur_line_pos + TABLE_CELL_SIZE;
		counter := (counter + 1) mod TABLE_CORNER_STEP;
	end;

	{ draw ordinary horyzontal lines }
	cur_line_pos := TABLE_CELL_SIZE - pos^.idt_y;
	counter := (TABLE_CORNER_STEP + 1 - pos^.y mod TABLE_CORNER_STEP)
		mod TABLE_CORNER_STEP; {writeln(pos^.y, ' ', counter);}
	while (cur_line_pos <= height - RULER_SIZE) do
	begin
		if (counter <> 0) then
		begin
			cairo_move_to(cr, 0, cur_line_pos);
			cairo_line_to(cr, width - RULER_SIZE, cur_line_pos);
		end;
		cur_line_pos := cur_line_pos + TABLE_CELL_SIZE;
		counter := (counter + 1) mod TABLE_CORNER_STEP;
	end;

	cairo_stroke(cr);


	cairo_set_source_rgb(cr, TABLE_CORNER_R,
		TABLE_CORNER_G, TABLE_CORNER_B);

	{ draw corner vertical lines }
	{ writeln(pos^.x, ' ', pos^.idt_x, ' | ',pos^.y, ' ', pos^.idt_y); }
	counter := ((-pos^.x) mod TABLE_CORNER_STEP + TABLE_CORNER_STEP)
		mod TABLE_CORNER_STEP;

	cur_line_pos := TABLE_CELL_SIZE * counter -	pos^.idt_x;
	if (counter = 0) then
		cur_line_pos := 
			cur_line_pos + 
			TABLE_CORNER_STEP * TABLE_CELL_SIZE;
	while (cur_line_pos <= width - RULER_SIZE) do
	begin
		cairo_move_to(cr, cur_line_pos, 0);
		cairo_line_to(cr, cur_line_pos, height - RULER_SIZE);
		cur_line_pos := cur_line_pos +
			TABLE_CORNER_STEP * TABLE_CELL_SIZE;
	end;

	counter := ((pos^.y) mod TABLE_CORNER_STEP + TABLE_CORNER_STEP)
		mod TABLE_CORNER_STEP;
	
	cur_line_pos := TABLE_CELL_SIZE * counter -	pos^.idt_y;
	if (counter = 0) then
		cur_line_pos := 
			cur_line_pos + 
			TABLE_CORNER_STEP * TABLE_CELL_SIZE;
	while (cur_line_pos <= height - RULER_SIZE) do
	begin
		cairo_move_to(cr, 0, cur_line_pos);
		cairo_line_to(cr, width - RULER_SIZE, cur_line_pos);
		cur_line_pos := cur_line_pos +
			TABLE_CORNER_STEP * TABLE_CELL_SIZE;
	end;
	
	cairo_stroke(cr);
	
	cairo_destroy(cr);
	on_expose_event := TRUE;
end;

function on_motion_notify_event	(	da:PGtkDrawingArea;
												event:PGdkEventMotion;
												pos:p_corner_position
											):gboolean; cdecl;
var
width, height:gint;
delta_x, delta_y:gdouble;
coord_line:pgchar;
begin
	{ writeln(event^.time); }

	gdk_drawable_get_size(GDK_DRAWABLE(event^.window), @width, @height);
	gtk_widget_queue_draw_area	(	GTK_WIDGET(da),
									0,
									0,
									width,
									RULER_SIZE
								);
	gtk_widget_queue_draw_area	(	GTK_WIDGET(da),
									0,
									0,
									RULER_SIZE,
									height
								);
								
	{ gtk_widget_queue_draw(GTK_WIDGET(da)); }
	
	if ((event^.state and GDK_BUTTON1_MASK) <> 0) then
	begin
		if (not pos^.on_moving) then
		begin
			pos^.prev_x := event^.x;
			pos^.prev_y := event^.y;
			pos^.on_moving := true;
		end
		else
		begin
			delta_x:=(event^.x - pos^.prev_x);
			delta_y:=(event^.y - pos^.prev_y);
			if	(	(abs(delta_x) >= SCROLLING_STEP) or
					(abs(delta_y) >= SCROLLING_STEP)
				) then
			begin
				{
				pos^.x :=	pos^.x +
							round	(int(	(delta_x - pos^.idt_x)
											/ TABLE_CELL_SIZE
										)
									);
				pos^.y :=	pos^.y -
							round	(int(	(delta_y - pos^.idt_y)
										/ TABLE_CELL_SIZE
									)
								);
				pos^.idt_x := 
					frac ((pos^.idt_x - delta_x) / TABLE_CELL_SIZE) *
							TABLE_CELL_SIZE;
				if (pos^.idt_x < 0) then
					pos^.idt_x := pos^.idt_x + TABLE_CELL_SIZE;
					
				pos^.idt_y :=
					frac ((pos^.idt_y - delta_y) / TABLE_CELL_SIZE) *
					TABLE_CELL_SIZE;
				if (pos^.idt_y < 0) then
					pos^.idt_y := pos^.idt_y + TABLE_CELL_SIZE;
				}
				
				pos^.idt_x := pos^.idt_x - delta_x;
				pos^.idt_y := pos^.idt_y - delta_y;
				
				if (pos^.idt_x > TABLE_CELL_SIZE) then
				begin
					pos^.x := pos^.x +
						round(trunc(pos^.idt_x / TABLE_CELL_SIZE));
					pos^.idt_x := frac(pos^.idt_x / TABLE_CELL_SIZE) *
						TABLE_CELL_SIZE;
				end
				else if (pos^.idt_x < 0) then
				begin
					pos^.x := pos^.x +
						round(trunc(pos^.idt_x / TABLE_CELL_SIZE)) - 1;
					pos^.idt_x := 
						(frac(pos^.idt_x / TABLE_CELL_SIZE) + 1) *
						TABLE_CELL_SIZE
				end;
				
				if (pos^.idt_y > TABLE_CELL_SIZE) then
				begin
					pos^.y := pos^.y -
						round(trunc(pos^.idt_y / TABLE_CELL_SIZE));
					pos^.idt_y := frac(pos^.idt_y / TABLE_CELL_SIZE) *
						TABLE_CELL_SIZE;
				end
				else if (pos^.idt_y < 0) then
				begin
					pos^.y := pos^.y -
						round(trunc(pos^.idt_y / TABLE_CELL_SIZE)) + 1;
					pos^.idt_y :=
						(frac(pos^.idt_y / TABLE_CELL_SIZE) + 1) *
						TABLE_CELL_SIZE
				end;
				
				pos^.prev_x := event^.x;
				pos^.prev_y := event^.y;
				pos^.on_moving := true;
				{writeln(pos^.x, ' ', pos^.y);}
				gtk_widget_queue_draw(GTK_WIDGET(da));
			end;

		end;
	end
	{else
		pos^.on_moving := false};
	
	{ resend coordinates string }
	if (not pos^.on_moving) then
	begin
		gtk_statusbar_pop(pos^.statusbar, pos^.statusbar_context_id);
		
		if ((event^.x >= RULER_SIZE) and (event^.y >= RULER_SIZE)) then
		begin
			coord_line := 
				g_strdup_printf(
					'x: %-lli' +'; ' +
					'y: %-lli',
					pos^.x + 
					round_to_smaler(
						(event^.x - RULER_SIZE + pos^.idt_x) /
						TABLE_CELL_SIZE
					),
					pos^.y -
					round_to_smaler(
						(event^.y - RULER_SIZE + pos^.idt_y) /
						TABLE_CELL_SIZE
					) - 1
					);
			gtk_statusbar_push(	pos^.statusbar,
								pos^.statusbar_context_id,
								coord_line
								);
			g_free(coord_line);
		end;
	end;
	
	on_motion_notify_event := FALSE;
end;

function on_leave_notify_event(da:PGtkDrawingArea;
								event:PGdkEventCrossing;
								pos:p_corner_position
								):gboolean; cdecl;
begin
	if (not pos^.on_moving) then
		gtk_statusbar_pop(pos^.statusbar, pos^.statusbar_context_id);
	
	on_leave_notify_event := FALSE;
end;

function on_button_release_event(	da:PGtkDrawingArea;
									event:PGdkEventButton;
									pos:p_corner_position
								):gboolean; cdecl;
var
width, height:gint;

pressed_x, pressed_y:gint64;
cell:p_cell_t;
y_cell_coord:gint64;

{i:byte;}
begin
	if	(	(event^._type = GDK_BUTTON_RELEASE) and
			(event^.button = 1)
		) then
	begin
		gdk_drawable_get_size(	GDK_DRAWABLE(event^.window),
								@width,
								@height
							);
		if	(	(event^.x < RULER_SIZE) or
				(event^.y < RULER_SIZE) or
				(event^.x > width) or
				(event^.y > height)
			) then
			gtk_statusbar_pop(	pos^.statusbar,
								pos^.statusbar_context_id
								);
		if (pos^.on_moving) then
			pos^.on_moving:= false
		else
		begin
			pressed_x := 	pos^.x + 
							round_to_smaler(
								(event^.x - RULER_SIZE + pos^.idt_x) /
								TABLE_CELL_SIZE
							);
			pressed_y :=	pos^.y -
							round_to_smaler(
								(event^.y - RULER_SIZE + pos^.idt_y) /
								TABLE_CELL_SIZE
							) - 1;
			
			cell := get_cell(	div_to_less(pressed_x,$10),
								div_to_less(pressed_y,$10),
								true);
			y_cell_coord := mod_to_less(pressed_y,$10);
			cell^.data[y_cell_coord] := cell^.data[y_cell_coord] xor
				($01 shl mod_to_less(pressed_x, $10));
			
			{
			writeln(pressed_x, ':', pressed_y);
			for i := 0 to 15 do
				writeln(cell^.data[i]);
			}
			
			gtk_widget_queue_draw(GTK_WIDGET(da));

		end;
	end;
	
	on_button_release_event := FALSE;
end;

procedure on_step_button_clicked(	button:PGtkButton;
									pos:p_corner_position
								); cdecl;
begin
	make_step;
	gtk_widget_queue_draw(GTK_WIDGET(pos^.da));
end;

procedure on_clear_button_clicked(	button:PGtkButton;
									pos:p_corner_position
								); cdecl;
begin
	gametable_clear;
	gtk_widget_queue_draw(GTK_WIDGET(pos^.da));
end;

procedure on_save_dialog_response(	dialog:PGtkFileChooserDialog;
									response_id:gint;
									data:pointer
								); cdecl;
var
	filename:pchar;
begin;
	if (response_id = GTK_RESPONSE_OK) then
	begin
		filename := gtk_file_chooser_get_filename
											(PGtkFileChooser(dialog));
		gametable_save(filename);
		g_free(filename);
		
		
	end;
	gtk_widget_destroy (GTK_WIDGET(dialog));
end;

procedure on_dialog_destroy(	dialog:PGtkFileChooserDialog;
									pos:p_corner_position
								); cdecl;
begin
	pos^.dialog_emitted := false;
	gtk_widget_set_sensitive(GTK_WIDGET(window), TRUE);
end;

procedure on_save_button_clicked(	button:PGtkButton;
									pos:p_corner_position
								); cdecl;
var
	chooser:PGtkWidget; {GtkFileChooserDialog}
begin
	if (not pos^.dialog_emitted) then
	begin
		chooser := gtk_file_chooser_dialog_new(
						nil,
						GTK_WINDOW(window),
						GTK_FILE_CHOOSER_ACTION_SAVE,
						GTK_STOCK_OK,
						GTK_RESPONSE_OK,
						GTK_STOCK_CANCEL,
						GTK_RESPONSE_CANCEL,
						nil
					);
					
		g_signal_connect(	chooser,
							'response',
							G_CALLBACK(@on_save_dialog_response),
							nil
						);
		g_signal_connect(	chooser,
							'destroy',
							G_CALLBACK(@on_dialog_destroy),
							pos
						);
		gtk_widget_set_sensitive(GTK_WIDGET(window), FALSE);
		
		pos^.dialog_emitted := true;
		
		gtk_widget_show(chooser);
	end;
end;

procedure on_open_dialog_response(	dialog:PGtkFileChooserDialog;
									response_id:gint;
									pos:p_corner_position
								); cdecl;
var
	filename:pchar;
begin
	if (response_id = GTK_RESPONSE_OK) then
	begin
		filename := gtk_file_chooser_get_filename
											(PGtkFileChooser(dialog));
		gametable_load(filename);
		g_free(filename);
		gtk_widget_queue_draw(GTK_WIDGET(pos^.da));
	end;
	gtk_widget_destroy (GTK_WIDGET(dialog));
end;


procedure on_open_button_clicked(	button:PGtkButton;
									pos:p_corner_position
								); cdecl;
var
	chooser:PGtkWidget; {GtkFileChooserDialog}
begin
	if (not pos^.dialog_emitted) then
	begin
		chooser := gtk_file_chooser_dialog_new(
					nil,
					GTK_WINDOW(window),
					GTK_FILE_CHOOSER_ACTION_OPEN,
					GTK_STOCK_OK,
					GTK_RESPONSE_OK,
					GTK_STOCK_CANCEL,
					GTK_RESPONSE_CANCEL,
					nil
				);
		g_signal_connect(	chooser,
							'response',
							G_CALLBACK(@on_open_dialog_response),
							pos
						);
		g_signal_connect(	chooser,
							'destroy',
							G_CALLBACK(@on_dialog_destroy),
							pos
						);
		
		gtk_widget_set_sensitive(GTK_WIDGET(window), FALSE);

		pos^.dialog_emitted := true;
		
		gtk_widget_show(chooser);
	end;
end;

{=========================== main =========================}

var
emask:TGdkEventMask;

pos:corner_position;

begin
	gtk_init(@argc, @argv);

	gametable_init;

	{widgets' initialization}
	window := gtk_window_new(GTK_WINDOW_TOPLEVEL);
	
	statusbar := gtk_statusbar_new;
	{writeln(G_IS_OBJECT(statusbar));}
	
	{actionbar := gtk_hbutton_box_new;}
	actionbar := gtk_hbox_new(FALSE,0);
	gotobutton := gtk_button_new_with_label('go to coordinates');
	
	step_button := gtk_button_new_with_label('Make a step');
	
	da := gtk_drawing_area_new;
	vbox := gtk_vbox_new(FALSE, MAIN_VBOX_SPACING);
	
	autostep_button := gtk_toggle_button_new_with_label ('Autostep');
	autostep_adjustment :=  gtk_adjustment_new(800, 10, 60000, 10, 0, 0);
	autostep_spin := gtk_spin_button_new(
				GTK_ADJUSTMENT(autostep_adjustment),
				1,
				0
			);
	
	clear_button := gtk_button_new_with_label('Clear gametable');
	
	save_button := gtk_button_new_from_stock(GTK_STOCK_SAVE);
	open_button := gtk_button_new_from_stock(GTK_STOCK_OPEN);
	
	{widget combining}
	{placing main vbox into the main window}
	gtk_container_add(GTK_CONTAINER(window), vbox);
	
	{packing horizontal fields into vbox}
	gtk_box_pack_start	(	GTK_BOX(vbox),
							da,
							TRUE,{expand}
							TRUE,{fill}
							TABLEWIDGET_PADDING
						);
	gtk_box_pack_start	(	GTK_BOX(vbox),
							actionbar,
							FALSE, {expand}
							FALSE, {fill}
							ACTIONBAR_PADDING
						);
	gtk_box_pack_start	(	GTK_BOX(vbox),
							statusbar,
							FALSE, {expand}
							FALSE, {fill}
							STATUSBAR_PADDING
						);
	
	{packing a button into actionbar}
	gtk_box_pack_start	(	GTK_BOX(actionbar),
							gotobutton,
							FALSE, {expand}
							FALSE, {fill}
							0
						);
	gtk_box_pack_start	(	GTK_BOX(actionbar),
							step_button,
							FALSE, {expand}
							FALSE, {fill}
							0
						);
	gtk_box_pack_start	(	GTK_BOX(actionbar),
							clear_button,
							FALSE, {expand}
							FALSE, {fill}
							0
						);
	gtk_box_pack_start	(	GTK_BOX(actionbar),
							save_button,
							FALSE, {expand}
							FALSE, {fill}
							0
						);
	gtk_box_pack_start	(	GTK_BOX(actionbar),
							open_button,
							FALSE, {expand}
							FALSE, {fill}
							0
						);
	gtk_box_pack_end	(	GTK_BOX(actionbar),
							autostep_spin,
							FALSE,
							FALSE,
							0
						);
	gtk_box_pack_end	(	GTK_BOX(actionbar),
							autostep_button,
							FALSE,
							FALSE,
							0
						);
	
	{setting "events" property}
	
	g_object_get(G_OBJECT(da), 'events', @emask, nil);
	emask :=	(	emask or
					GDK_POINTER_MOTION_MASK or
					GDK_BUTTON_MOTION_MASK or
					GDK_BUTTON_PRESS_MASK or
					GDK_BUTTON_RELEASE_MASK or
					GDK_POINTER_MOTION_HINT_MASK or
					GDK_LEAVE_NOTIFY_MASK
				);
			{GDK_ALL_EVENTS_MASK;}
	g_object_set(G_OBJECT(da), 'events', emask, nil);	
	
	{set statusbar resize grip}
	{g_object_set(G_OBJECT(statusbar), 'has-resize-grip', FALSE, nil);}
	
	{adding signals}
	corner_position_init(	@pos,
							GTK_STATUSBAR(statusbar),
							GTK_DRAWING_AREA(da),
							GTK_ADJUSTMENT(autostep_adjustment)
						);
	
	g_signal_connect(window, 'destroy', G_CALLBACK(@on_destroy), nil);
	g_signal_connect(da, 'expose-event', G_CALLBACK(@on_expose_event),
		@pos);
	g_signal_connect(da, 'motion-notify-event',
		G_CALLBACK(@on_motion_notify_event), @pos);
	{g_signal_connect(da, 'destroy', G_CALLBACK(@on_da_destroy), @pos);}
	g_signal_connect(	da,
						'leave-notify-event',
						G_CALLBACK(@on_leave_notify_event),
						@pos
					);
	g_signal_connect(	da,
						'button-release-event',
						G_CALLBACK(@on_button_release_event),
						@pos
					);
	
	{set button params}
	gotobutton_params.main_window := GTK_WINDOW(window);
	
	gotobutton_params.set_coords_proc :=
		set_coords_proc_t(@corner_position_set_xy);

	gotobutton_params.get_coords_proc :=
		get_coords_proc_t(@corner_position_get_xy);
	
	gotobutton_params.get_dialog_emitted :=
		get_dialog_emitted_t(@corner_position_get_dialog_emitted);
	
	gotobutton_params.set_dialog_emitted :=
		set_dialog_emitted_t(@corner_position_set_dialog_emitted);
	
	gotobutton_params.proc_data := @pos;
	
	g_signal_connect(	gotobutton,
						'clicked',
						G_CALLBACK(@on_set_coords_button),
						@gotobutton_params
					);
	
	g_signal_connect(	step_button,
						'clicked',
						G_CALLBACK(@on_step_button_clicked),
						@pos
					);
	g_signal_connect(	clear_button,
						'clicked',
						G_CALLBACK(@on_clear_button_clicked),
						@pos
					);
	g_signal_connect(	autostep_button,
						'toggled',
						G_CALLBACK(@timer_toggled),
						@pos
					);
	g_signal_connect(	save_button,
						'clicked',
						G_CALLBACK(@on_save_button_clicked),
						@pos
					);
	g_signal_connect(	open_button,
						'clicked',
						G_CALLBACK(@on_open_button_clicked),
						@pos
					);
				
	gtk_widget_show_all(window);
	
	gtk_main;
	
	corner_position_deinit(@pos);
	
	gametable_deinit;
	
end.
