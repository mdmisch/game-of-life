Unit gametable

Interface

const max_addr = $7FFF;

Implementation

type
p_chunk=^chunk;
ch_types = (ch_init, ch_punct, ch_transit, ch_floor);

chunk = record
	ch_type:ch_types;
	parent:p_chunk;
end;

ch_punct = record
	header : chunk;
	childs : array [0..255] of p_chunk;
end;

ch_init = record
	header : chunk;
	n_e : p_chunk;
	n_e_lvl : word;
	n_w : p_chunk;
	n_w_lvl : word;
	s_w: p_chunk;
	s_w_lvl : word;
	s_e : p_chunk;
	s_e_lvl : word;
end;

p_ch_init = ^ch_init;

addr_array = array [0..0] of byte;
p_addr_array = ^addr_array;

ch_transit = record
	header : chunk;
	length : word; {0 означає масив в 1 байт}
	chain : p_addr_array;
	child : p_chunk;
end;

ch_floor = record
	header : chunk;
	state: array [0..15] of word;
	history: array [0..15] of word;
end;

p_ch_floor = ^ch_floor;

address = record
	sign : boolean;
	length : word;
	address : array [0..0] byte;
end;

p_address = ^address;

function get_part_of_address(x,y:p_address ; level:word):byte;
	var x_val,y_val:byte
	begin
		if (x = nil) or (level >= (x^.length * 2)) then
			x_val := 0
		else
		begin
			x_val := x^.address[level shl 1]
			if ((level and 1) = 1) then
				x_val := x_val shr 4
			else
				x_val := x_val and $0F;
		end;
		
		if (y = nil) or (level >= (y^.length * 2)) then
			y_val := 0
		else
		begin
			y_val := y^.address[level shl 1]
			if ((level and 1) = 1) then
				y_val := y_val and $F0
			else
				y_val := y_val shl 4;
		end;
		
		get_part_of_address := x_val or y_val;
	end;
	
function get_border_chunk(	init : p_chunk;
							x,y : p_address;
							var cur_level : word;
						):p_chunk;
	cur_chunk : p_chunk;
	{Поточний чанк, який обробляємо зараз. Повинен відповідати
	* рівню level}
	init_level : word;
	{Рівень, на якому знаходться потрібна нам сторона світу}
	
	in_transit_level : word;
	{Використовується для відліку при проходженні
	* поточного транзитного чанку}
	
	tmp_addr : byte;
	{ Використовується для тимчасового зберігання фрагменту адреси
	* при проходженні звичайного чанка.}
	
	begin
		if (init^.ch_type = ch_init) then
			begin
				if ((not x^.sign) and (not y^.sign)) then
					begin
						cur_chunk := ch_init(init^).n_e;
						init_level := ch_init(init^).n_e_lvl;
					end
				else if (x^.sign and (not y^.sign))
					begin
						cur_chunk := ch_init(init^).n_w;
						init_level := ch_init(init^).n_w_lvl;	
					end
				else if (x^.sign and y^.sign)
					begin
						cur_chunk := ch_init(init^).s_w;
						init_level := ch_init(init^).s_w_lvl;	
					end
				else if ((not x^.sign) and y^.sign)
					begin
						cur_chunk := ch_init(init^).s_e;
						init_level := ch_init(init^).s_e_lvl;	
					end;
				{Обираємо правильну сторону світу}
				
				if (cur_chunk = nil) then
					begin
						get_border_chunk := init;
						exit;
					end;
				{Якщо cur_chunk = nil, виходимо.}
				
				if (x^.length > y^.length) then
					cur_level := x^.length shl 1
				else
					cur_level := y^.length shl 1
				
				if (cur_level < init_level) then
					cur_level := init_level;
				
				while 	(cur_level > init_level) and
						(get_part_of_address(x,y,curlevel) = 0) do
					dec (cur_level);
				
				if (cur_level > init_chunk) then
					begin
						get_barder_chunk := init;
						exit;
					end;
				{Якщо шукана частина дошки знаходиться поза
				* максимальним наявним рівнем, виходимо.}
			end
		else
			cur_chunk:=init;
			
		while	true do
		begin
			if (cur_chunk^.ch_types = ch_punct) then
				begin
					tmp_addr := get_part_of_address(x,y,cur_level);
					if	(	ch_punct(cur_chunk^).childs
								[tmp_addr] <> nil
						) then
						begin
							cur_chunk := ch_punct(cur_chunk^).childs
															[tmp_addr];
							if (cur_level = 0) then
								begin
									writeln(stderr, 
										'Chunk tree corruption: '
										'punct shunk at level 0'
										);
									halt;
								end;
							dec(cur_level)
						end
					else 
						break;
					{Якщо звичайний чанк, спускаємось вниз,
					* якщо це можливо.}
				end
			else if (cur_chunk^.ch_types = ch_transit) then
				begin
					in_transit_level := cur_level;
					
					if 	(	in_transit_level <= 
							tr_chunk(cur_chunk^).length
						) then
						begin
							writeln	(stderr, 'Chunk tree corruption: '
										'transit chunk length is too '
										'big'
									);
							halt;										
						end;
					
					if (tr_chunk(cur_chunk^).child = nil) then
						break;
					
					with tr_chunk(cur_chunk^) do
						while 	(in_transit_level >= 
								 (cur_level - length)
								) and
								((chain^[length + cur_level - 
												in_transit_level]) =
									get_part_of_address(x,y,cur_level)
								) do
								dec(in_transit_level);
					if	(	in_transit_level <
							(cur_level - tr_chunk(cur_chunk^).length)
						) then
						begin
							cur_chunk := tr_chunk(cur_chunk^).child;
							cur_level :=
								cur_level - tr_chunk(cur_chunk^) - 1;
						end
					else
						break;
				end
			else if (cur_chunk^.ch_types = ch_floor) then
				begin
					if (cur_level = 0) then
						break
					else
						begin
							writeln(stderr, 'Chunk tree corruption: '
								'floor chunk not on the 0 level');
							halt;
						end;
				end;
			else
				begin
					writeln(stderr, 'Chunk tree corruption: '
						'bad chunk type');
					halt;
				end;
		end;
		get_boarder_chunk := cur_chunk;
	end;
	
function get_floor_chunk(	init : p_ch_init;
							x,y : p_address;
						):p_ch_floor;
	var
	ret_chunk : p_chunk;
	cur_level : word;
	begin
		ret_chunk := get_border_chunk(init,x,y,cur_level);
		
		if	(cur_level <> 0) or
			(ret_chunk = nil) or
			(ret_chunk^.ch_type = ch_init) then
			get_floor_chunk := nil;
		else
			get_floor_chunk := ret_chunk;
	end;

function init_ch_floor(parent:p_chunk):p_ch_floor;
	var
	new_chunk:p_ch_floor;
	i:byte;
	begin
		new_chunk:=malloc(sizeof(ch_floor));
		new_chunk^.header.ch_type:=ch_floor;
		new_chunk^.header.parent:=parent;
		for i:= 0 to 15 do
			head[i]:=0;
		for i:= 0 to 15 do
			history[i]:=0;
		init_ch_floor:=new_chunk;
	end;

function init_ch_transit	(	x,y:p_addressl;
								top,bottom:word;
								parent:p_chunk;
								child:p_chunk;
							):p_ch_transit;
	var
	new_chunk:p_ch_transit;
	begin
		new_chunk:=malloc(sizeof(ch_transit))
		new_chunk^.header.ch_type:=ch_transit;
		new_chunk^.header.parent:=parent;
		new_chunk^.length := top - bottom;
		new_chunk^.chain :=
			malloc	( sizeof(byte) * (top + 1 - bottom) );
			
		for i in top to bottom do
			new_chunk^.chain^[i - bottom] :=
				get_part_of_address(x, y, i);
		new_chunk^.child:=
			init_ch_floor(new_chunk);
		ch_punct(cur_chunk^).childs
			[get_part_of_address(x, y, cur_level)] := child;
		
		init_ch_transit := new_chunk;
	end;


function generate_floor_chunk	(	init : p_chunk;
									x,y : p_address;
									var cur_level;
								):p_ch_floor;
								
	var
	cur_chunk:p_chunk;
	new_chunk:p_chunk;
	i:word;
	begin
		cur_chunk:=get_border_chunk(init,x,y,cur_level);
		
		if (cur_chunk^.ch_type = floor) then
			generate_floor_chunk:=cur_chunk
		else if (cur_chunk^.ch_type = ch_punct) then
			begin
				if (cur_level = 1) then
					begin
						new_chunk:=init_ch_floor(cur_chunk);
						
						ch_punct(cur_chunk^).childs
							[get_part_of_address(x, y, cur_level)]
								:= new_chunk;
						generate_floor_chunk:=new_chunk;
							
					end;
				else
					begin
						new_chunk :=	init_ch_transit(
											x,y,
											cur_level - 1,1,
											cur_chunk,
											nil
										);
						ch_transit(new_chunk^).child := 
											init_ch_floor(new_chunk);
						
						ch_punct(cur_chunk^).childs
							[get_part_of_address(x, y, cur_level)] :=
								new_chunk;
						
						generate_floor_chunk:=new_chunk^.child;
					end;
			end;
		else if (cur_chunk^.ch_type = ch_transit) then
			begin
				for i := cur_level to 
						(cur_level - ch_transit(cur_chunk^).level) do
						if	(	ch_transit(cur_chunk^).chain^
									[cur_level + i - 
									ch_transit(cur_chunk^).level] !=
								get_part_of_address(x, y, i)
							) then
							break
				{...}			
					
			end
		else if (cur_chunk^.ch_type = ch_init)
			begin
				{...}
			end
		else
			begin
				{...}
			end;
		
		
	end;
